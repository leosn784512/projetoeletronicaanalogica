import cv2
import sys
import numpy as np
import cv2

class ImageProcessing:

    INPUT_WIDTH = 352
    INPUT_HEIGHT = 352
    SCORE_THRESHOLD = 0.2
    NMS_THRESHOLD = 0.4
    CONFIDENCE_THRESHOLD = 0.4

    WEIGHTS = "weights/yolov5nnnn.onnx"

    def __init__(self, path_class_list = "weights/classes.txt") -> None:
        self.class_list = self.loadClasses(path_class_list)
        pass

    def markImageBoundingBox(self, frame, classid, confidence, box, depth):
        colors = [(255, 255, 0), (0, 255, 0), (0, 255, 255), (255, 0, 0)]

        for (class_id, confidencee, boxx) in zip(classid, confidence, box):
            color = colors[int(class_id) % len(colors)]
            
            cv2.rectangle(frame, boxx, color, 2)
            cv2.rectangle(frame, (boxx[0], boxx[1] - 20), (boxx[0] + boxx[2], boxx[1]), color, -1)
   
            distance = depth[boxx[0]+int((boxx[2] - boxx[0]) / 2)][boxx[1]+int(((boxx[3] - boxx[1]) / 2))]
            if (distance / 10) > 0:
                cv2.circle(frame, (boxx[0]+int((boxx[2] - boxx[0]) / 2), (boxx[1]+int((boxx[3] - boxx[1]) / 2)) + 10), 4, (255, 0, 0), 4)
                cv2.putText(frame, str(int(distance/10)) + " cm", (boxx[0]+int((boxx[2] - boxx[0]) / 2), boxx[1]+int(((boxx[3] - boxx[1]) / 2))), cv2.FONT_HERSHEY_SIMPLEX, .6, (255,0,0), 2)

            cv2.putText(frame, self.class_list[class_id], (boxx[0], boxx[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, .5, (0,0,0))

        return frame

    def buildModel(self):
        # Check if there is a cuda
        is_cuda = len(sys.argv) > 1 and sys.argv[1] == "cuda"

        # Loading model
        net = cv2.dnn.readNet(self.WEIGHTS)

        # Defining what kind of processor will be used
        if is_cuda:
            net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
            net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA_FP16)
        else:
            net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
            net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)
        return net

    def detectObjectOnImage(self, image, net):
        blob = cv2.dnn.blobFromImage(image, 1/255.0, (self.INPUT_WIDTH, self.INPUT_HEIGHT), swapRB=True, crop=False)
        net.setInput(blob)
        preds = net.forward()
        return preds

    def loadCapture(self):
        capture = cv2.VideoCapture("assets/videos/sample.mp4")
        return capture

    def loadClasses(self, path_class_list):
        class_list = []
        with open(path_class_list, "r") as f:
            class_list = [cname.strip() for cname in f.readlines()]
        return class_list

    def wrapDetection(self, input_image, output_data):
        class_ids = []
        confidences = []
        boxes = []

        rows = output_data.shape[0]

        image_width, image_height, _ = input_image.shape

        x_factor = image_width / self.INPUT_WIDTH
        y_factor =  image_height / self.INPUT_HEIGHT

        for r in range(rows):
            row = output_data[r]
            confidence = row[4]
            if confidence >= 0.4:

                classes_scores = row[5:]
                _, _, _, max_indx = cv2.minMaxLoc(classes_scores)
                class_id = max_indx[1]
                if (classes_scores[class_id] > .25):

                    confidences.append(confidence)

                    class_ids.append(class_id)

                    x, y, w, h = row[0].item(), row[1].item(), row[2].item(), row[3].item() 
                    left = int((x - 0.5 * w) * x_factor)
                    top = int((y - 0.5 * h) * y_factor)
                    width = int(w * x_factor)
                    height = int(h * y_factor)
                    box = np.array([left, top, width, height])
                    boxes.append(box)

        # Nonmax suppression to remove bounding boxes overlaping with the same class

        indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.25, 0.45) 

        result_class_ids = []
        result_confidences = []
        result_boxes = []

        for i in indexes:
            result_confidences.append(confidences[i])
            result_class_ids.append(class_ids[i])
            result_boxes.append(boxes[i])

        return result_class_ids, result_confidences, result_boxes

    def formatImageYoloV5(self, frame):
        row, col, _ = frame.shape
        _max = max(col, row)
        result = np.zeros((_max, _max, 3), np.uint8)
        result[0:row, 0:col] = frame
        return result


